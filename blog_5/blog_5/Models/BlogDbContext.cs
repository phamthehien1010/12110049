﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace blog_5.Models
{
    public class BlogDbContext : DbContext 
    {
        public DbSet<Post> Posts { set; get; }
        public DbSet<Tag> Tags { set; get; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Post>().HasMany(t => t.Tags)
                .WithMany(p => p.Posts).Map(k => k.MapLeftKey("PostID").MapRightKey("TagID").ToTable("Tag_Post"));
        }
    }
}