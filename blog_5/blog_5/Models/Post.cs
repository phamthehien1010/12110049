﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace blog_5.Models
{
    public class Post
    {
        public int ID { set; get; }
        public String Tittle { set; get; }
        public String Body { set; get; }
        public DateTime DateCreated { set; get; }

        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}