﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models
{
    [Table("BaiViet")]
    public class Post
    {
        [Range(2,100)]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public int ID { set; get; }
        [Required]
        public String Title { set; get; }
        [StringLength(250,ErrorMessage="So luong ky tu trong khoang 10 - 250",MinimumLength=10)]
        public String Body { set; get; }

        [DataType(DataType.Date)]
        public DateTime DateCreated { set; get; }
        public DateTime DateUpdated { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        public int AccountID { set; get; }
        public virtual Account Accounts { set; get; }
    }
}